# 2019-SECGY-PHY-GUI
## 한국어(Korean)
### 소개
* 2019년 순천대 물리 영재원 GUI 계산 프로그램 
### 파이썬
* 의존성: PySerial, PyQt5 , Numpy 라이브러리  
* 테스트된 작동 환경: Fedora 30(데스크탑 환경: Gnome) , windows 10
* python 3 에서만 정상 작동 됩니다.
### 예시 아두이노 코드
* 사용된 광센서는 OPT819Z 입니다.
* 진자운동 코드에서 딜레이를 필요시 조절해주세요.
* Arduino UNO R3 보드에서 작동이 테스트 되었습니다.
### 실행방법 (알파 버전: 테스트되지 않음)
* linux
```
    ~$ sh install.sh
```
* windows
```
    > install.bat
```
### 회로도
* 회로도는 KiCAD에서 작성되었습니다.
## English
### introduction
* 2019 SECGY physics department GUI calculation software  
### Python
* dependency : PySerial, PyQt5 , Numpy library  
* tested environment: Fedora 30(Desktop Environment: Gnome) , windows 10
* only python 3 is supported
### example Arduino Code
* photoswitch used in this project is OPT819Z
* in pendulum code, please adjust delay as needed
* code is tested in Arduino Uno R3
### how to use (Alpha Version: NOT TESTED)
* linux
```
    ~$ sh install.sh
```
* windows
```
    > install.bat
```
### schematic
* schematic is written with KiCAD

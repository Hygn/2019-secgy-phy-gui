from dat_rcv import *
import sys
import numpy as np
from PyQt5.QtCore import Qt
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
class Window(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        layout = QGridLayout()
        self.setWindowTitle("SECGY-PHY")
        self.setWindowIcon(QIcon(QPixmap("resources/workmeu.res")))
        def btn(label,value,ontoggle,ypos,xpos,objname,checkable,checked):
            button = self.button_s = QPushButton(label, objectName=objname)
            button.value = value
            button.setCheckable(checkable)
            button.setChecked(checked)
            button.clicked.connect(ontoggle)
            layout.addWidget(button,ypos,xpos)
        def labeltag(labeltxt,ypos,xpos,objname,value):
            label = self.label_s = QLabel(labeltxt,objectName=objname)
            label.value = value
            layout.addWidget(label,ypos,xpos)
        def radiobtn(label,value,ontoggle,ypos,xpos,objname):
            radiobutton = QRadioButton(label, objectName=objname)
            radiobutton.value = value
            radiobutton.toggled.connect(ontoggle)
            layout.addWidget(radiobutton,ypos,xpos)
        def textbox(value,ypos,xpos,objname):
            textbox= self.textbox_s = QLineEdit(self,objectName=objname)
            textbox.value = value
            layout.addWidget(textbox,ypos,xpos)
        self.setLayout(layout)
        labeltag("arduino serial port path",0,0,"typelabel","port")
        textbox("path",1,0,"path")
        btn("connect","connect",self.retriveSerial,2,0,"connect",False,True)
        #experiment type
        labeltag("experiment type",3,0,"typelabel_lh","")
        radiobtn("Free Fall","freefall",self.onClicked,4,0,"exptype")
        radiobtn("Pendulum","pendulum",self.onClicked,5,0,"exptype")
        radiobtn("Slide","slide",self.onClicked,6,0,"exptype")
        #sensor location
        labeltag("input params",7,0,"typelabel_lh","")
        labeltag("location#1",8,0,"typelabel_nom1","")
        textbox("input#1",8,1,"input1")
        labeltag("location#2",9,0,"typelabel_nom2","")
        textbox("input#2",9,1,"input2")
        labeltag("location#3",10,0,"typelabel_nom3","")
        textbox("input#3",10,1,"input3")
        labeltag("noinput",11,0,"typelabel_nom4","")
        textbox("input#4",11,1,"input4")
        #experiment start
        labeltag("ready for experiment",12,0,"typelabel_lh","")
        btn("ready for another","readyexp",self.retriveSerial,13,0,"button",False,True)
        #export data
        labeltag("result data",14,0,"typelabel_lh","data")
        labeltag("",15,0,"typelabel_res","dataout")
    def onClicked(self):
        extype = self.sender()
        global exptype
        if extype.isChecked() and self.button_s.value == 'readyexp':
            print("SECGY-PHY-GUI[information]: toggle: experiment type " + extype.value)
            exptype = extype.value
            self.button_s.setCheckable(True)
            if exptype == "freefall":
                self.findChild(QLabel, "typelabel_nom1").setText("location#1")
                self.findChild(QLabel, "typelabel_nom2").setText("location#2")
                self.findChild(QLabel, "typelabel_nom3").setText("location#3")
                self.findChild(QLabel, "typelabel_nom4").setText("noinput")
            if exptype == "slide":
                self.findChild(QLabel, "typelabel_nom1").setText("location#1")
                self.findChild(QLabel, "typelabel_nom2").setText("location#2")
                self.findChild(QLabel, "typelabel_nom3").setText("location#3")
                self.findChild(QLabel, "typelabel_nom4").setText("angle")
            if exptype == "pendulum":
                self.findChild(QLabel, "typelabel_nom1").setText("length")
                self.findChild(QLabel, "typelabel_nom2").setText("noinput")
                self.findChild(QLabel, "typelabel_nom3").setText("noinput")
                self.findChild(QLabel, "typelabel_nom4").setText("noinput")
        if extype.isChecked() != True:
            print("SECGY-PHY-GUI[information]: untoggle: experiment type " + extype.value)
    def retriveSerial(self):
        exstart = self.sender()
        btype = exstart.value
        if exstart.isChecked() != True and btype == "connect":
            print("SECGY-PHY-GUI[information]: click: connect")
            ttyport = self.findChild(QLineEdit, "path").text()
            if ttyport != "work" and ttyport != "":
                serial_cn = serial_conn(ttyport,9600,0.08)
                if serial_cn == "connectionerror":
                    pass
                else:
                    exstart.setText("connected")
                    exstart.setStyleSheet("color: green; border-left-color: green")
            if ttyport == "work":
                print("SECGY-PHY-GUI[information]: exec: easter egg discovered! workmeu!")
                messagebox = QMessageBox(QMessageBox.Warning, "workmeu!", "I love coding meu!", 
                objectName="coding")
                messagebox.addButton(QPushButton('예??'), QMessageBox.YesRole)
                messagebox.addButton(QPushButton('예'), QMessageBox.NoRole)
                messagebox.setIconPixmap(QPixmap("resources/workmeu.res"))
                messagebox.exec_()
            if ttyport == "":
                print("SECGY-PHY-GUI[error]: error: please input the path")
        if exstart.isChecked() and btype == "readyexp":
            print("SECGY-PHY-GUI[information]: click: ready for another")
            data = b''
            while exstart.isChecked() and data != b'TXEND' and data != "nameerror":
                data = serial_read()
                if data != b'':
                    extractdata = str(data).replace("b'",'').replace("'",'')
                    if extractdata.split(":")[0] == "RES" and extractdata.count("RES:") == 1:
                        print("SECGY-PHY-GUI[information]: receive: " + extractdata)
                        if self.label_s.value == "dataout":
                            opt = ""
                            if exptype == "freefall":
                                datas = extractdata.replace("RES:",'').split(",")
                                X1 = float(self.findChild(QLineEdit, "input1").text())
                                X2 = float(self.findChild(QLineEdit, "input2").text())
                                X3 = float(self.findChild(QLineEdit, "input3").text())
                                dX21 = X2 - X1
                                dX32 = X3 - X2
                                dX31 = X3 - X1
                                dT21 = (float(datas[1]) - float(datas[0])) / float(1000000)
                                dT32 = (float(datas[2]) - float(datas[1])) / float(1000000)
                                dT13 = dT21 + dT32
                                pri0 = 2 * ( dX32 * dT21 - dX21 * dT32 )
                                pri1 = dT21 * dT32 * dT13
                                gacc = pri0/pri1
                                opt = "time between 1,2 : " + str(dT21) + "\ntime between 2,3 : " + str(dT32) + "\ntime between 1,3 : " + str(dT13) + "\ngravitational accl : " + str(gacc)
                                print("SECGY-PHY-GUI[information]: result: \n" + opt)
                            if exptype == "slide":
                                datas = extractdata.replace("RES:",'').split(",")
                                angle = float(self.findChild(QLineEdit, "input4").text())
                                X1 = float(self.findChild(QLineEdit, "input1").text())
                                X2 = float(self.findChild(QLineEdit, "input2").text())
                                X3 = float(self.findChild(QLineEdit, "input3").text())
                                dX21 = X2 - X1
                                dX32 = X3 - X2
                                dX31 = X3 - X1
                                dT21 = (float(datas[1]) - float(datas[0])) / float(1000000)
                                dT32 = (float(datas[2]) - float(datas[1])) / float(1000000)
                                dT13 = dT21 + dT32
                                sin = np.sin(angle * np.pi / 180.)
                                pri0 = 2 * ( dX32 * dT21 - dX21 * dT32 )
                                pri1 = dT21 * dT32 * dT13
                                accl = pri0/pri1
                                gacc = (3 * accl) / (2 * sin)
                                opt = "time between 1,2 : " + str(dT21) + "\ntime between 2,3 : " + str(dT32) + "\ntime between 1,3 : " + str(dT13) + "\nangle : " + str(angle) + "\naccl : " + str(accl) + "\nsine : " + str(sin) + "\ngravitational accl : " + str(gacc)
                                print("SECGY-PHY-GUI[information]: result: \n" + opt)
                            if exptype == "pendulum":
                                datas = extractdata.replace("RES:",'').split(",")
                                length = float(self.findChild(QLineEdit, "input1").text())
                                period = (float(datas[2]) - float(datas[0])) / float(1000000)
                                gacc = (4 * pow(np.pi, 2) * length)/ pow(period, 2)
                                opt = "\nperiod : " + str(period) + "\ngravitational accl : " + str(gacc)
                                print("SECGY-PHY-GUI[information]: result: \n" + opt)
                            self.label_s.setText(opt)
                            exstart.setChecked(False)
                        else:
                            pass
        if exstart.isChecked() != True and btype == "readyexp":
            print("SECGY-PHY-GUI[information]: unclick: ready for another")
app = QApplication(sys.argv)
app.setStyle("fusion")
app.setStyleSheet(open("main.css","r").read())
screen = Window()
screen.show()
sys.exit(app.exec_())
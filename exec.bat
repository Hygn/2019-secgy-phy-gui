@ECHO off
echo installing dependencies
python3 -m pip install --upgrade pip
python3 -m pip install pyqt5 --no-warn-script-location
python3 -m pip install pyserial --no-warn-script-location
python3 -m pip install numpy --no-warn-script-location
echo complete! executing main app
python3 app_gui.py
echo main app terminated.
import serial
def serial_conn(ttyport,baud,to):
    global ser
    try:
        ser = serial.Serial(ttyport, baud, timeout=to)
    except:
        print("[dat_rcv module] error: ConnectionError")
        return("connectionerror")
def serial_read():
    try:
        dataout = ser.readline()
        return(dataout)
    except NameError :
        print("[dat_rcv module] error: NameError")
        return("nameerror")